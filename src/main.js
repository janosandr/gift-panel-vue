import Vue from "vue";
import App from "./App.vue";
import VueResource from "vue-resource";
// import "./registerServiceWorker";
import VueRouter from "vue-router";
import router from "./routes";
import store from "./store";

// import store from "./store";

Vue.config.productionTip = false;
Vue.use(VueRouter);
Vue.use(VueResource);

new Vue({
  // store,
  render: h => h(App),

  store,
  router
}).$mount("#app");
