export default {
    USER(state){
        return state.user;
    },
    USERS(state){
        return state.users;
    },
    GET_USER: (state) => (id) => {
        return state.users.data.find(user => user.id === id)
    },
    CATEGORIES(state){
        return state.categories;
    },
    CATEGORY(state){
        return state.category
    },
    GET_CATEGORY: (state) => (id) => {
        return state.categories.data.find(category => category.id == id)
    },
    PRODUCTS(state){
        return state.products;
    }
}