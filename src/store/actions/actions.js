export default {
    SET_USER_TO_STATE(state, user) {
      state.user = user;
    },
    SET_USERS_TO_STATE(state, users){
      state.users = users;
    },
    SET_CATEGORIES_TO_STATE(state, categories) {
        state.categories = categories;
    },
    SET_CATEGORY_TO_STATE(state, category){
      state.category = category;
    },
    SET_PRODUCTS_TO_STATE(state,products){ 
        state.products = products;
    },
    updateUser: ({commit, state}, user) => {
      commit('SET_USER_TO_STATE', user)
      return state.user;
    },
    updateCategory: ({commit, state}, category) => {
      commit('SET_CATEGORY_TO_STATE', category)
      return state.category;
    }
  };
  