import axios from "axios"
const watchToken        = document.location.search.substr(7,32)
// const getToken       = window.localStorage.getItem('token');

const baseURL           = 'https://api.giftmarket.io';

const adminInit         = '/admin/init?token='
const adminGet          = '/admin/admins/get?token='
// const adminAdd       = '/admin/admins/add?token='
// const adminRemove    = '/admin/admins/remove?token='
// const admineEdit     = '/admin/admins/edit?token='
const catsGet           = '/admin/categories/get?token=' 
const productsGet       = '/admin/products/get?token=' 

const token = localStorage.token


export default {
    GET_USER_FROM_API({commit}) {
        return axios(baseURL + adminInit + watchToken, {
            method: "GET"
        })
        .then((user) => {
            window.localStorage.setItem('token',watchToken)
            commit('SET_USER_TO_STATE', user.data)
            return user;
        })
    },
    GET_USERS_FROM_API({commit}) {
        return axios(baseURL + adminGet + token, {
            method: "GET"
        })
        .then((users) => {
            commit('SET_USERS_TO_STATE', users.data)
            return users;
        })
    },
    GET_CATEGORIES_FROM_API({commit}) {
        return axios(baseURL + catsGet + token, {
            method: "GET"
        })
        .then((categories) => {
            commit('SET_CATEGORIES_TO_STATE', categories.data.data)
            return categories;
        })
    },
    GET_CATEGORY_FROM_API({commit}) {
        return(baseURL + catsGet + token, {
            method: "GET"
        })
        .then((category) => {
            commit('SET_CATEGORY_TO_STATE', category.data.data)
            return category;
        })
    },
    GET_PRODUCTS_FROM_API({commit}) {
        return axios(baseURL + productsGet + token, {
            method: "GET"
        })
        .then((products) => {
            commit('SET_PRODUCTS_TO_STATE', products.data.data)
            return products;
        })
    },
    async loadUsers({commit}, users) {
        const response = await axios.get(users.src)
        commit('updateUsers', response.data)
      }
}