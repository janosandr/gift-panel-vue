import VueRouter from "vue-router";
import Home from "./pages/Home";
import Admin from "./pages/Admin";
import Login from "./pages/Login";
import UserEdit from "./pages/UserEdit";
import AddUser from "./pages/AddUser";
import Gifts from "./pages/Gifts";
import AddGift from "./pages/AddGift";
import GiftEdit from "./pages/GiftEdit";
import editCategory from "./pages/editCategory";
import ErrorCmp from "./pages/Error";
import Categories from "./pages/Categories";


export default new VueRouter({
  routes: [
    {
      path: "/",
      component: Login
    },
    {
      path: "/admin",
      component: Admin,
      name: "admin"
    },
    {
      path: "/home",
      component: Home
    },
    {
      path: "/categories",
      component: Categories
    },
    {
      path: "/category-edit/:id",
      component: editCategory,
      props: true
    },
    {
      path: "/adduser",
      component: AddUser
    },
    {
      path: "/gifts",
      component: Gifts,
    },
    {
      path: "/addgift",
      component: AddGift
    },
    {
      path: "/edit/:id",
      component: UserEdit,
      props: true
    },
    {
      path: "/product-edit/:id",
      component: GiftEdit,
      props: true
    },
     {
      path: "*",
      component: ErrorCmp
    }
  ],
  mode: "history"
});
